﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogDAL.Models
{
    public class Tag : BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string TagName { get; set; }

        public virtual ICollection<Article> Articles { get; set; }
    }
}