﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogDAL.Models
{
    public class Comment : BaseEntity
    {
        [ForeignKey("Article")]
        public int ArticleId { get; set; }
        public Article Article { get; set; }

        [ForeignKey("User")]
        public int? UserId { get; set; }
        public UserProfile User { get; set; }

        public string CommentText { get; set; }
    }
}