﻿using System;
using Microsoft.AspNetCore.Identity;

namespace BlogDAL.Models
{
    public class User : IdentityUser<int>
    {
        public UserProfile UserProfile { get; set; }
    }
}