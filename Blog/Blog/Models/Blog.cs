﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogDAL.Models
{
    public class Blog : BaseEntity
    {
        [ForeignKey("User")]
        public int UserId { get; set; }
        public UserProfile User { get; set; }
        
        public virtual ICollection<Article> Articles { get; set; }
    }
}