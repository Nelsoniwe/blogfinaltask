﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogDAL.Models
{
    public class Article : BaseEntity
    {
        [ForeignKey("Blog")]
        public int BlogId { get; set; }
        public Blog Blog { get; set; }

        [Required]
        public DateTime PublishDate { get; set; }

        [Required]
        [StringLength(1500, MinimumLength = 50)]
        public string Body { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 20)]
        public string Title { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}