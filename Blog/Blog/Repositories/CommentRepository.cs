﻿using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BlogDAL.Repositories
{
    /// <summary>
    /// Repository to work with comments
    /// </summary>
    public class CommentRepository : IRepository<Comment>
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly BlogContext _db;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="db">Database context</param>
        public CommentRepository(BlogContext db)
        {
            _db = db;
        }
        public async Task AddAsync(Comment entity)
        {
           await _db.Comments.AddAsync(entity);
        }

        public void DeleteById(int id)
        {
            var comment = _db.Comments.Find(id);
            if (comment!=null)
            {
                _db.Comments.Remove(comment);
            }
        }

        public async Task<IQueryable<Comment>> GetAllAsync()
        {
            var comments = await _db.Comments.ToListAsync();
            return comments.AsQueryable();
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _db.Comments.FindAsync(id);
        }

        public void Update(Comment entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}