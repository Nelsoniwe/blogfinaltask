﻿using BlogDAL.Interfaces;
using System.Threading.Tasks;
using BlogDAL.Models;
using Microsoft.AspNetCore.Identity;

namespace BlogDAL.Repositories
{
    /// <summary>
    /// Repository to work with roles
    /// </summary>
    public class RoleRepository : IRoleRepository
    {
        /// <summary>
        /// Manager for managing roles
        /// </summary>
        private readonly RoleManager<Role> _roleManager;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="roleManager">Role manager</param>
        public RoleRepository(RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await _roleManager.RoleExistsAsync(roleName);
        }
    }
}