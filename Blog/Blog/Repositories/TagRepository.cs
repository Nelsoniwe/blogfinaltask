﻿using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BlogDAL.Repositories
{
    /// <summary>
    /// Repository to work with tags
    /// </summary>
    public class TagRepository : IRepository<Tag>
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly BlogContext _db;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="db">Database context</param>
        public TagRepository(BlogContext db)
        {
            _db = db;
        }
        public async Task AddAsync(Tag entity)
        {
            await _db.AddAsync(entity);
        }

        public void DeleteById(int id)
        {
            var tag = _db.Tags.Find(id);
            if (tag != null)
            {
                _db.Tags.Remove(tag);
            }
        }

        public async Task<IQueryable<Tag>> GetAllAsync()
        {
            var tags = await _db.Tags.ToListAsync();
            return tags.AsQueryable();
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _db.Tags.SingleOrDefaultAsync(x=>x.Id==id);
        }

        public void Update(Tag entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}