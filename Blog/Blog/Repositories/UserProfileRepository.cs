﻿using BlogDAL.Interfaces;
using BlogDAL.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;

namespace BlogDAL.Repositories
{
    /// <summary>
    /// Repository to work with user profiles
    /// </summary>
    public class UserProfileRepository : IUserProfileRepository
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly BlogContext _db;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="db">Database context</param>
        public UserProfileRepository(BlogContext db)
        {
            _db = db;
        }

        public async Task<IQueryable<UserProfile>> GetAllWithDetailsAsync()
        {
            var users = await _db.UserProfiles
                .Include(x => x.Blog)
                .Include(x => x.AppUser)
                .ToListAsync();
            return users.AsQueryable();
        }

        public async Task<UserProfile> GetByIdAsync(int id)
        {
            return await _db.UserProfiles.FindAsync(id);
        }

        public async Task<UserProfile> GetByIdWithDetailsAsync(int id)
        {
            return await _db.UserProfiles
                .Include(x => x.Blog).ThenInclude(x=>x.Articles)
                .Include(x => x.AppUser)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}