﻿using BlogDAL.Interfaces;
using BlogDAL.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BlogDAL.Repositories
{
    /// <summary>
    /// Repository to work with article
    /// </summary>
    public class ArticleRepository : IArticleRepository
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly BlogContext _db;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="db">Database context</param>
        public ArticleRepository(BlogContext db)
        {
            _db = db;
        }

        public async Task AddAsync(Article entity)
        {
            await _db.Articles.AddAsync(entity);
        }

        public void DeleteById(int id)
        {
            var article = _db.Articles.Find(id);
            if (article != null)
            {
                 _db.Articles.Remove(article);
            }
        }

        public async Task<IQueryable<Article>> GetAllAsync()
        {
            var articles = await _db.Articles.ToListAsync();
            return articles.AsQueryable();
        }

        public async Task<IQueryable<Article>> GetAllWithDetailsAsync()
        {
            var articles = await _db.Articles.Include(x=>x.Comments).Include(x=>x.Tags).ToListAsync();
            return articles.AsQueryable();
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            return await _db.Articles.FindAsync(id);
        }

        public async Task<Article> GetByIdWithDetailsAsync(int id)
        {
            return await _db.Articles.Include(x=>x.Comments).Include(x=>x.Tags).FirstOrDefaultAsync(x=>x.Id == id);
        }

        public void Update(Article entity)
        {
            var article = _db.Articles.FirstOrDefault(x=>x.Id == entity.Id);
            article.Tags = entity.Tags;
            article.Body = entity.Body;
            article.PublishDate = entity.PublishDate;
            article.Title = entity.Title;
        }
    }
}