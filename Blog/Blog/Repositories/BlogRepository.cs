﻿using BlogDAL.Interfaces;
using BlogDAL.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BlogDAL.Repositories
{
    /// <summary>
    /// Repository to work with blogs
    /// </summary>
    public class BlogRepository : IBlogRepository
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly BlogContext _db;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="db">Database context</param>
        public BlogRepository(BlogContext db)
        {
            _db = db;
        }

        public async Task AddAsync(Blog entity)
        {
            await _db.Blogs.AddAsync(entity);
        }

        public void DeleteById(int id)
        {
            var blog = _db.Blogs.Find(id);
            if (blog!=null)
            {
                _db.Remove(blog);
            }
        }

        public async Task<IQueryable<Blog>> GetAllAsync()
        {
            var blogs = await _db.Blogs.ToListAsync();
            return blogs.AsQueryable();
        }

        public async Task<IQueryable<Blog>> GetAllWithDetailsAsync()
        {
            var blogs = await _db.Blogs.Include(x=>x.Articles).ToListAsync();
            return blogs.AsQueryable();
        }

        public async Task<Blog> GetByIdAsync(int id)
        {
            return await _db.Blogs.FindAsync(id);
        }

        public async Task<Blog> GetByIdWithDetailsAsync(int id)
        {
             return await _db.Blogs.Include(x=>x.Articles).FirstOrDefaultAsync(x =>x.Id == id);
        }

        public void Update(Blog entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}