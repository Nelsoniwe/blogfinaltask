﻿using System.Threading.Tasks;
using BlogDAL.Interfaces;
using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;

namespace BlogDAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BlogContext _db;

        public UnitOfWork(BlogContext db, IArticleRepository articleRepository, IBlogRepository blogRepository, IRepository<Comment> commentRepository,
            IRoleRepository roleRepository, IRepository<Tag> tagRepository, IUserProfileRepository userProfileRepository, IUserRepository userRepository)
        {
            _db = db;
            ArticleRepository = articleRepository;
            BlogRepository = blogRepository;
            CommentRepository = commentRepository;
            RoleRepository = roleRepository;
            TagRepository = tagRepository;
            UserProfileRepository = userProfileRepository;
            UserRepository = userRepository;
        }
        public IArticleRepository ArticleRepository { get; }
        public IBlogRepository BlogRepository { get ; }
        public IRepository<Comment> CommentRepository { get; }
        public IRoleRepository RoleRepository { get; }
        public IRepository<Tag> TagRepository { get; }
        public IUserProfileRepository UserProfileRepository { get; }
        public IUserRepository UserRepository { get ; }

        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync();
        }
    }
}