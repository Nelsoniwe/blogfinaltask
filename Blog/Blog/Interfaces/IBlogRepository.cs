﻿using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;

namespace BlogDAL.Interfaces
{
    /// <summary>
    /// Repository interface to work with blogs
    /// </summary>
    public interface IBlogRepository : IRepository<Blog>, IRepositoryExpanded<Blog>
    {
        
    }
}