﻿using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;

namespace BlogDAL.Interfaces
{
    /// <summary>
    /// Repository interface to work with articles
    /// </summary>
    public interface IArticleRepository : IRepository<Article>,IRepositoryExpanded<Article>
    {
        
    }
}