﻿using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL.Interfaces.BaseInterfaces
{
    /// <summary>
    /// UOF Interface
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Article repository
        /// </summary>
        IArticleRepository ArticleRepository { get; }
        /// <summary>
        /// Blog repository
        /// </summary>
        IBlogRepository BlogRepository { get; }
        /// <summary>
        /// Comment repository
        /// </summary>
        IRepository<Comment> CommentRepository { get; }
        /// <summary>
        /// Role repository
        /// </summary>
        IRoleRepository RoleRepository { get; }
        /// <summary>
        /// Tag repository
        /// </summary>
        IRepository<Tag> TagRepository { get; }
        /// <summary>
        /// User profile repository
        /// </summary>
        IUserProfileRepository UserProfileRepository { get; }
        /// <summary>
        /// User repository
        /// </summary>
        IUserRepository UserRepository { get; }
        /// <summary>
        /// Asynchronously save db
        /// </summary>
        Task<int> SaveAsync();
    }
}