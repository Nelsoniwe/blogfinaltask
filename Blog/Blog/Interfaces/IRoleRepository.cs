﻿using System.Threading.Tasks;

namespace BlogDAL.Interfaces
{
    /// <summary>
    /// Repository interface to work with roles
    /// </summary>
    public interface IRoleRepository
    {
        /// <summary>
        /// Check if role exist or not
        /// </summary>
        /// <param name="roleName">Role name</param>
        /// <returns>true if role exist and false if not</returns>
        Task<bool> RoleExistsAsync(string roleName);
    }
}