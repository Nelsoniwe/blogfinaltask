﻿using System.Threading.Tasks;
using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;

namespace BlogDAL.Interfaces
{
    /// <summary>
    /// Repository interface to work with user profiles
    /// </summary>
    public interface IUserProfileRepository : IRepositoryExpanded<UserProfile>
    {
        /// <summary>
        /// Asynchronously gets user
        /// </summary>
        /// <param name="id">Id of user that should be found</param>
        /// <returns>User profile info <see cref="UserProfile"/></returns>
        Task<UserProfile> GetByIdAsync(int id);
    }
}