﻿using System;
using BlogDAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlogDAL
{
    public class BlogContext : IdentityDbContext<User, Role, int>
    {
        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {
        }

        //public BlogContext(DbContextOptions options) : base(options)
        //{
        //}

        //public BlogContext() : base()
        //{

        //}

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=localhost,1433;Database=NewDataBase;User ID=sa;Password=Str0ngPa$$w0rd");
        //}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);


            var adminRole = new Role { Id = 1, Name = "Admin", NormalizedName = "ADMIN" };
            var userRole = new Role { Id = 2, Name = "User", NormalizedName = "USER" };

            builder.Entity<Role>().HasData(adminRole, userRole);

            var adminData = new User
            {
                Id = 1,
                Email = "admin@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                UserName = "Admin",
                NormalizedUserName = "ADMIN"
            };

            var adminProfile = new UserProfile
            {
                Id = adminData.Id,
                FirstName = "Admin",
                LastName = "Admin"
            };

            var userData = new User
            {
                Id = 2,
                Email = "user@gmail.com",
                NormalizedEmail = "USER@GMAIL.COM",
                UserName = "User",
                NormalizedUserName = "USER"
            };

            var userProfile = new UserProfile
            {
                Id = userData.Id,
                FirstName = "User",
                LastName = "User"
            };

            var hasher = new PasswordHasher<User>();
            var adminPassword = hasher.HashPassword(adminData, "A1234");

            adminData.PasswordHash = adminPassword;

            builder.Entity<UserProfile>().HasData(adminProfile);
            builder.Entity<User>().HasData(adminData);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int> { RoleId = adminRole.Id, UserId = adminProfile.Id },
                new IdentityUserRole<int> { RoleId = userRole.Id, UserId = adminProfile.Id });

            var userPassword = hasher.HashPassword(userData, "D1234");
            userData.PasswordHash = userPassword;

            builder.Entity<UserProfile>().HasData(userProfile);
            builder.Entity<User>().HasData(userData);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int> { RoleId = userRole.Id, UserId = userProfile.Id });

            var tags = new[]
            {
                new Tag { Id = 1, TagName = "Detective" },
                new Tag { Id = 2, TagName = "Horror" },
                new Tag { Id = 3, TagName = "Comedy" },
                new Tag { Id = 4, TagName = "Fantasy" },
                new Tag { Id = 5, TagName = "Adventure" }
            };

            builder.Entity<Comment>()
                .HasOne(e => e.User)
                .WithMany(e=>e.Comments)
                .OnDelete(deleteBehavior: DeleteBehavior.NoAction);

            builder.Entity<Article>().HasMany(x=>x.Tags).WithMany(x=>x.Articles);
            builder.Entity<Tag>().HasData(tags);
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
    }
}