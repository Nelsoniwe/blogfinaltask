﻿using AutoMapper;
using BlogBLL.Models;
using BlogDAL.Models;
using BlogPL.Models;

namespace BlogPL.Mappers
{
    public class AutoMapperPL:Profile
    {
        public AutoMapperPL()
        {
            CreateMap<UserDTO, RegisterModel>()
                .ReverseMap();
            CreateMap<TagModel, TagDTO>()
                .ReverseMap();
            CreateMap<ArticleModel, ArticleDTO>()
                .ReverseMap();
            CreateMap<ArticleUpdateModel, ArticleDTO>()
                .ReverseMap();
            CreateMap<CommentModel, CommentDTO>()
                .ReverseMap();
            CreateMap<UserModel, UserDTO>()
                .ReverseMap();
        }
    }
}