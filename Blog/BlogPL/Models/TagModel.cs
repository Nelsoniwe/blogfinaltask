﻿using System.ComponentModel.DataAnnotations;

namespace BlogPL.Models
{
    public class TagModel
    {
        [Required]
        public string TagName { get; set; }
    }
}