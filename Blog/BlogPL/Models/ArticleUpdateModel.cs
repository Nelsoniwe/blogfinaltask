﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogPL.Models
{
    public class ArticleUpdateModel
    {
        [Required]
        public int id { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        public string Title { get; set; }

        public ICollection<TagModel> Tags { get; set; }
    }
}