﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogPL.Models
{
    public class ArticleModel
    {
        public int id { get; }

        [Required]
        public string Body { get; set; }

        [Required]
        public string Title { get; set; }

        public ICollection<TagModel> Tags { get; set; }
    }
}