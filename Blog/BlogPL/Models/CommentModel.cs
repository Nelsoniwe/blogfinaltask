﻿using System.ComponentModel.DataAnnotations;

namespace BlogPL.Models
{
    public class CommentModel
    {
        [Required]
        public int ArticleId { get; set; }
        [Required]
        public string CommentText { get; set; }
    }
}