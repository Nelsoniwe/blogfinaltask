﻿using System.Net;
using BlogBLL.Exceptions;
using BlogPL.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogPL.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        public ActionResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;

            if (exception is BlogException)
                return BadRequest(new ErrorModel(exception.Message, HttpStatusCode.BadRequest));

            if (exception is NotFoundException)
                return BadRequest(new ErrorModel(exception.Message, HttpStatusCode.NotFound));

            return new JsonResult(new ErrorModel(exception.Message, HttpStatusCode.InternalServerError));
        }
    }
}