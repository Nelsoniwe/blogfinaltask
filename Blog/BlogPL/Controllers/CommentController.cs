﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogBLL.Interfaces;
using BlogBLL.Models;
using BlogPL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace BlogPL.Controllers
{
    /// <summary>
    /// Controller for work with comments
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public CommentController(ICommentService commentService, IMapper mapper, IUserService userService)
        {
            _commentService = commentService;
            _mapper = mapper;
            _userService = userService;
        }

        /// <summary>
        /// Get comment by id
        /// </summary>
        [HttpGet]
        [Route("GetById")]
        public async Task<ActionResult<CommentDTO>> GetCommentById(int id)
        {
            return Ok(await _commentService.GetCommentById(id));
        }

        /// <summary>
        /// Get all article comments by its id
        /// </summary>
        /// <param name="id">Article id that comments should be found</param>
        [HttpGet]
        [Route("GetByArticleId")]
        public async Task<ActionResult<IEnumerable<CommentDTO>>> GetCommentsByArticleId(int id)
        {
            return Ok(await _commentService.GetAllCommentsByArticleId(id));
        }

        /// <summary>
        /// Get all user comments by its id
        /// </summary>
        /// <param name="id">User id that comments should be found</param>
        [HttpGet]
        [Route("GetByUserId")]
        public async Task<ActionResult<IEnumerable<CommentDTO>>> GetCommentByUserId(int id)
        {
            return Ok(await _commentService.GetAllCommentsByUserId(id));
        }

        /// <summary>
        /// Add new comment to article
        /// </summary>
        /// <param name="comment">Comment info with text and article id</param>
        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> CreateComment([FromBody] CommentModel comment)
        {
            var commentToAdd = _mapper.Map<CommentDTO>(comment);
            commentToAdd.UserId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var newCommentId = await _commentService.AddComment(commentToAdd);
            return Ok(newCommentId);
        }

        /// <summary>
        /// Delete comment from article
        /// </summary>
        /// <param name="id">Comment id that should be deleted</param>
        [HttpDelete]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> DeleteComment(int id)
        {
            var roles = await _userService.UserGetRoles(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));

            if (roles.Any(x => x == "Admin"))
            {
                await _commentService.DeleteCommentById(id);
                return Ok();
            }

            var comment = await _commentService.GetCommentById(id);

            if (comment.UserId != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return BadRequest("It is not your comment");

            await _commentService.DeleteCommentById(id);
            return Ok();
        }

    }
}
