﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Interfaces;
using BlogBLL.Models;
using BlogPL.Models;
using Microsoft.AspNetCore.Authorization;

namespace BlogPL.Controllers
{
    /// <summary>
    /// Controller for work with blogs
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IBlogService _blogService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public BlogController(IBlogService blogService, IMapper mapper, IUserService userService)
        {
            _blogService = blogService;
            _mapper = mapper;
            _userService = userService;
        }

        /// <summary>
        /// Get all Blogs
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<BlogDTO>>> GetAllBlogs()
        {
            return Ok(await _blogService.GetAllBlogsWithDetails());
        }

        /// <summary>
        /// Get user blog by its username
        /// </summary>
        /// <param name="name">The name of the user whose blog should be found</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByUserName")]
        public async Task<ActionResult<BlogDTO>> GetBlogByUserName(string name)
        {
            return Ok(await _blogService.GetBlogByUserName(name));
        }

        /// <summary>
        /// Get user blog by its id
        /// </summary>
        /// <param name="id">The id of the user whose blog should be found</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByUserId")]
        public async Task<ActionResult<BlogDTO>> GetBlogByUserId(int id)
        {
            return Ok(await _blogService.GetBlogByUserId(id));
        }

        /// <summary>
        /// Get blog by id
        /// </summary>
        /// <param name="id">The id of blog that should be found</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetById")]
        public async Task<ActionResult<BlogDTO>> GetBlogById(int id)
        {
            return Ok(await _blogService.GetBlogByIdWithDetails(id));
        }

        /// <summary>
        /// Create blog for authorized user
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> CreateBlog()
        {
            var newBlogId = await _blogService.CreateBlog(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));
            return Ok(await _blogService.GetBlogByIdWithDetails(newBlogId));
        }

        /// <summary>
        /// Delete blog for authorized user
        /// </summary>
        [HttpDelete]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> DeleteBlog(int id)
        {
            var blog = await _blogService.GetBlogById(id);
            var roles = await _userService.UserGetRoles(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));

            if (roles.Any(x => x == "Admin"))
            {
                await _blogService.DeleteBlogById(id);
                return Ok();
            }

            var user = await _userService.GetById(blog.UserId);

            if (user.Id != Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return BadRequest("It is not your blog");


            await _blogService.DeleteBlogById(blog.Id);
            return Ok();
        }
    }
}
