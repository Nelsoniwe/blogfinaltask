﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Interfaces;
using BlogBLL.Models;
using BlogDAL.Models;
using BlogPL.Models;
using Microsoft.AspNetCore.Authorization;

namespace BlogPL.Controllers
{
    /// <summary>
    /// Controller for work with articles
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleService _articleService;
        private readonly IBlogService _blogService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public ArticleController(IArticleService articleService, IMapper mapper, IUserService userService, IBlogService blogService)
        {
            _articleService = articleService;
            _mapper = mapper;
            _userService = userService;
            _blogService = blogService;
        }

        /// <summary>
        /// Get all articles
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetAll")]
        public async Task<ActionResult<IEnumerable<ArticleDTO>>> GetAllArticles()
        {
            return Ok(await _articleService.GetAllArticlesWithDetails());
        }

        /// <summary>
        /// Get all articles by tag
        /// </summary>
        /// <param name="tag">Tag which article should be found of</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByTag")]
        public async Task<ActionResult<IEnumerable<ArticleDTO>>> GetArticlesByTag(string tag)
        {
            return Ok(await _articleService.GetAllArticlesByTag(tag));
        }

        /// <summary>
        /// Get all articles by text in body
        /// </summary>
        /// <param name="text">Text in body which article should be found of</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByText")]
        public async Task<ActionResult<IEnumerable<ArticleDTO>>> GetArticlesByText(string text)
        {
            return Ok(await _articleService.GetAllArticlesByText(text));
        }

        /// <summary>
        /// Get all articles by user id
        /// </summary>
        /// <param name="id">The id of the user whose articles should be found</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByUserId")]
        public async Task<ActionResult<IEnumerable<ArticleDTO>>> GetArticlesByUserId(int id)
        {
            return Ok(await _articleService.GetAllArticlesByUserId(id));
        }


        
        /// <summary>
        /// Get articles by id
        /// </summary>
        /// <param name="id">The id of the article that should be found</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetById")]
        public async Task<ActionResult<IEnumerable<ArticleDTO>>> GetArticleById(int id)
        {
            var article = await _articleService.GetArticleByIdWithDetails(id);
            if (article == null)
            {
                return NotFound();
            }
            return Ok(article);
        }

        /// <summary>
        /// Get all articles by user name
        /// </summary>
        /// <param name="name">The name of the user whose articles should be found</param>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetByUserName")]
        public async Task<ActionResult<IEnumerable<ArticleDTO>>> GetArticlesByUserName(string name)
        {
            return Ok(await _articleService.GetAllArticlesByUserName(name));
        }

        /// <summary>
        /// Add new article to authorized user
        /// </summary>
        /// <param name="article">Article info that should be added</param>
        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> AddArticle([FromBody] ArticleModel article)
        {
            var articleToAdd = _mapper.Map<ArticleDTO>(article);
            var userBlog = await _blogService.GetBlogByUserId(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));
            articleToAdd.BlogId = userBlog.Id;
            var articleId = await _articleService.AddArticle(articleToAdd);
            return Ok(await _articleService.GetArticleById(articleId));
        }

        /// <summary>
        /// Delete article by authorized user
        /// </summary>
        /// <param name="id">Article id that should be deleted</param>
        [HttpDelete]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> DeleteArticleById(int id)
        {
            var roles = await _userService.UserGetRoles(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));
            if (roles.Any(x => x == "Admin"))
            {
                await _articleService.DeleteArticleById(id);
                return Ok();
            }

            var articleToDelete = await _articleService.GetArticleById(id);
            var userBlog = await _blogService.GetBlogByUserId(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));

            if (articleToDelete.BlogId != userBlog.Id)
                return BadRequest("It is not your article");

            await _articleService.DeleteArticleById(id);
            return Ok();
        }

        /// <summary>
        /// Update article by authorized user
        /// </summary>
        /// <param name="article">Article info that should be updated</param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> UpdateArticle([FromBody] ArticleUpdateModel article)
        {
            var articleToUpdate = await _articleService.GetArticleById(article.id);
            var userBlog = await _blogService.GetBlogByUserId(Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier).Value));

            if (articleToUpdate == null)
                return BadRequest("Article not found");

            if (articleToUpdate.BlogId != userBlog.Id)
                return BadRequest("It is not your article");
            
            await _articleService.UpdateArticle(_mapper.Map<ArticleDTO>(article));

            return Ok(await _articleService.GetArticleById(article.id));
        }
    }
}
