﻿using System;
using BlogBLL.Interfaces;
using BlogBLL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogBLL.Services
{
    /// <summary>
    /// Service to work with articles
    /// </summary>
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public ArticleService(IUnitOfWork uow, IMapper mapper)
        {
            _db = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Add new article
        /// </summary>
        /// <param name="article"><see cref="ArticleDTO"/> new article info</param>
        /// <returns>New article id</returns>
        /// <exception cref="BlogException">Throws when title, body or tag were empty</exception>
        public async Task<int> AddArticle(ArticleDTO article)
        {
            if (string.IsNullOrEmpty(article.Title) || string.IsNullOrEmpty(article.Body))
            {
                throw new BlogException("Title or Body were empty");
            }

            var tags = new List<Tag>();
            var allTags = await _db.TagRepository.GetAllAsync();

            foreach (var tag in article.Tags)
            {
                var tagFromDB = allTags.FirstOrDefault(x => x.TagName == tag.TagName);
                tags.Add(tagFromDB ?? _mapper.Map<Tag>(tag));
            }

            var articleToDb = _mapper.Map<Article>(article);
            articleToDb.Tags = tags;
            articleToDb.PublishDate = DateTime.Now;

            await _db.ArticleRepository.AddAsync(articleToDb);
            await _db.SaveAsync();
            return articleToDb.Id;
        }

        /// <summary>
        /// Delete article by id
        /// </summary>
        /// <param name="articleId">article id</param>
        public async Task DeleteArticleById(int articleId)
        {
            _db.ArticleRepository.DeleteById(articleId);
            await _db.SaveAsync();
        }

        /// <summary>
        /// Get all articles with details
        /// </summary>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> Articles</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllArticles()
        {
            return _mapper.Map<IEnumerable<ArticleDTO>>(await _db.ArticleRepository.GetAllAsync());
        }

        /// <summary>
        /// Get all articles by tag
        /// </summary>
        /// <param name="tag">tag </param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticleDTOs</returns>
        /// <exception cref="NotFoundException">Throws when tag not found in db</exception>
        public async Task<IEnumerable<ArticleDTO>> GetAllArticlesByTag(string tagName)
        {
            var tag = (await _db.TagRepository.GetAllAsync()).FirstOrDefault(x => x.TagName == tagName);
            if (tag == null)
                throw new NotFoundException("Tag not found");
            return _mapper.Map<IEnumerable<ArticleDTO>>((await _db.ArticleRepository.GetAllWithDetailsAsync()).Where(x => x.Tags.Contains(tag)));
        }

        /// <summary>
        /// Get all articles by text in body
        /// </summary>
        /// <param name="tag">tag </param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticleDTOs</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllArticlesByText(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new BlogException("Text cannot be empty");

            return _mapper.Map<IEnumerable<ArticleDTO>>((await _db.ArticleRepository.GetAllWithDetailsAsync()).Where(x => x.Body.Contains(text)));
        }

        /// <summary>
        /// Get all articles by user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticlesDTOs</returns>
        /// <exception cref="NotFoundException">Throws when user or blog not found in db</exception>
        public async Task<IEnumerable<ArticleDTO>> GetAllArticlesByUserId(int userId)
        {
            var user = await _db.UserRepository.GetByIdAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found");

            var userBlog = (await _db.BlogRepository.GetAllAsync()).FirstOrDefault(x => x.UserId == userId);

            if (userBlog == null)
                throw new NotFoundException("Blog not found");

            return _mapper.Map<IEnumerable<ArticleDTO>>(await _db.ArticleRepository.GetAllWithDetailsAsync()).Where(x => x.BlogId == userBlog.Id);
        }

        /// <summary>
        /// Get all articles by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticlesDTOs</returns>
        /// <exception cref="NotFoundException">Throws when user or blog not found in db</exception>
        public async Task<IEnumerable<ArticleDTO>> GetAllArticlesByUserName(string userName)
        {
            var user = await _db.UserRepository.GetAllWithDetails().FirstOrDefaultAsync(x => x.UserName == userName);
            if (user == null)
                throw new NotFoundException("User not found");

            var userBlog = await (await _db.BlogRepository.GetAllAsync()).FirstOrDefaultAsync(x => x.UserId == user.UserProfile.Id);

            if (userBlog == null)
                throw new NotFoundException("Blog not found");

            return _mapper.Map<IEnumerable<ArticleDTO>>(await _db.ArticleRepository.GetAllWithDetailsAsync()).Where(x => x.BlogId == userBlog.Id);
        }

        /// <summary>
        /// Get all articles
        /// </summary>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> Articles</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllArticlesWithDetails()
        {
            return _mapper.Map<IEnumerable<ArticleDTO>>(await _db.ArticleRepository.GetAllWithDetailsAsync());
        }

        /// <summary>
        /// Update Article info
        /// </summary>
        /// <param name="article">article with id and new info</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        public async Task UpdateArticle(ArticleDTO article)
        {
            var articleFromDB = await _db.ArticleRepository.GetByIdWithDetailsAsync(article.Id);
            article.PublishDate = articleFromDB.PublishDate;
            if (articleFromDB == null)
                throw new NotFoundException("Article not found");


            if (string.IsNullOrEmpty(article.Title) || string.IsNullOrEmpty(article.Body))
                throw new BlogException("Title or Body were empty");
   

            var tags = new List<Tag>();
            var allTags = await _db.TagRepository.GetAllAsync();


            foreach (var tag in article.Tags)
            {
                var tagFromDB = allTags.FirstOrDefault(x => x.TagName == tag.TagName);
                tags.Add(tagFromDB ?? _mapper.Map<Tag>(tag));
            }

            var articleToDb = _mapper.Map<Article>(article);
            articleToDb.Tags = tags;
            _db.ArticleRepository.Update(articleToDb);
            await _db.SaveAsync();
        }
        

        /// <summary>
        /// Get article by id
        /// </summary>
        /// <param name="id">article id</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        public async Task<ArticleDTO> GetArticleById(int id)
        {
            return _mapper.Map<ArticleDTO>(await _db.ArticleRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Get article by id with details
        /// </summary>
        /// <param name="id">article id</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        public async Task<ArticleDTO> GetArticleByIdWithDetails(int id)
        {
            return _mapper.Map<ArticleDTO>(await _db.ArticleRepository.GetByIdWithDetailsAsync(id));
        }

        /// <summary>
        /// Add new tag to Article
        /// </summary>
        /// <param name="articleId">article id where we want to add tag in</param>
        /// <param name="tag">tag name</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        public async Task AddTag(int articleId, string tag)
        {
            var articleFromDB = _mapper.Map<ArticleDTO>(await _db.ArticleRepository.GetByIdWithDetailsAsync(articleId));

            if (string.IsNullOrEmpty(tag))
                throw new BlogException("tag were empty");

            if (articleFromDB == null)
                throw new NotFoundException("Article not found");

            if (articleFromDB.Tags.FirstOrDefault(x => x.TagName == tag) != null)
                throw new BlogException("Article already have this tag");

            var allTags = await _db.TagRepository.GetAllAsync();

            TagDTO newTag = new TagDTO(){TagName = tag};

            if (await allTags.FirstOrDefaultAsync(x => x.TagName == newTag.TagName) == null)
                await _db.TagRepository.AddAsync(_mapper.Map<Tag>(newTag));

            articleFromDB.Tags.Add(newTag);

            _db.ArticleRepository.Update(_mapper.Map<Article>(articleFromDB));
            await _db.SaveAsync();
        }
    }
}