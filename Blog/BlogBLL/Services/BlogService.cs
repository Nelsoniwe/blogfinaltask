﻿using BlogBLL.Interfaces;
using BlogBLL.Models;
using BlogDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogBLL.Services
{
    /// <summary>
    /// Service to work with blogs
    /// </summary>
    public class BlogService : IBlogService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public BlogService(IUnitOfWork uow, IMapper mapper)
        {
            _db = uow;
            _mapper = mapper;
        }
        /// <summary>
        /// Create blog
        /// </summary>
        /// <param name="userId">user id whose blog to create</param>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        /// <exception cref="BlogException">Throws when user blog already exist in db</exception>
        public async Task<int> CreateBlog(int userId)
        {
            var user = await _db.UserProfileRepository.GetByIdWithDetailsAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found");
            if (user.Blog != null)
                throw new BlogException("User already have blog");
            var newBlog = new Blog() { UserId = userId };
            await _db.BlogRepository.AddAsync(newBlog);
            await _db.SaveAsync();
            return newBlog.Id;
        }
        /// <summary>
        /// Delete blog by id
        /// </summary>
        /// <param name="blogId">blog id</param>
        /// <exception cref="NotFoundException">Throws when blog not found in db</exception>
        public async Task DeleteBlogById(int blogId)
        {
            if (await _db.BlogRepository.GetByIdAsync(blogId) == null)
                throw new NotFoundException("Blog not found");
            _db.BlogRepository.DeleteById(blogId);
            await _db.SaveAsync();
        }
        /// <summary>
        /// Get all blogs with details
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> BlogDTOs</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllBlogsWithDetails()
        {
            return _mapper.Map<IEnumerable<BlogDTO>>(await _db.BlogRepository.GetAllWithDetailsAsync());
        }

        /// <summary>
        /// Get all blogs 
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> BlogDTOs</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllBlogs()
        {
            return _mapper.Map<IEnumerable<BlogDTO>>(await _db.BlogRepository.GetAllAsync());
        }

        /// <summary>
        /// Get blog by id
        /// </summary>
        /// <param name="blogId">blog id</param>
        /// <returns><see cref="BlogDTO"/></returns>
        public async Task<BlogDTO> GetBlogById(int blogId)
        {
            var blog = await _db.BlogRepository.GetByIdAsync(blogId);
            if (blog == null)
                throw new NotFoundException("Blog not found");
            return _mapper.Map<BlogDTO>(blog);
        }

        /// <summary>
        /// Get blog with details
        /// </summary>
        /// <param name="id">blog id</param>
        /// <returns><see cref="BlogDTO"/> info</returns>
        /// <exception cref="NotFoundException">Throws when blog not found in db</exception>
        public async Task<BlogDTO> GetBlogByIdWithDetails(int id)
        {
            var blog = _mapper.Map<BlogDTO>(await _db.BlogRepository.GetByIdWithDetailsAsync(id));
            if (blog == null)
                throw new NotFoundException("Blog not found");
            return blog;
        }
        /// <summary>
        /// Get blog by user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns><see cref="BlogDTO"/></returns>
        /// <exception cref="NotFoundException">Throws when user or blog not found in db</exception>
        public async Task<BlogDTO> GetBlogByUserId(int userId)
        {
            var user = await _db.UserProfileRepository.GetByIdWithDetailsAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found");
            if (user.Blog == null)
                throw new NotFoundException("Blog not found");
            return _mapper.Map<BlogDTO>(user.Blog);
        }
        /// <summary>
        /// Get blog by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns><see cref="BlogDTO"/></returns>
        /// <exception cref="NotFoundException">Throws when user or blog not found in db</exception>
        public async Task<BlogDTO> GetBlogByUserName(string userName)
        {
            var user = _db.UserRepository.GetAllWithDetails().FirstOrDefault(x => x.UserName == userName);

            if (user == null)
                throw new NotFoundException("User not found");

            var userBlog = (await _db.BlogRepository.GetAllWithDetailsAsync()).FirstOrDefault(x => x.UserId == user.UserProfile.Id);

            if (userBlog == null)
                throw new NotFoundException("Blog not found");

            return _mapper.Map<BlogDTO>(userBlog);
        }
    }
}