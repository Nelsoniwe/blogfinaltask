﻿using BlogBLL.Interfaces;
using BlogBLL.Models;
using BlogDAL.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogDAL.Interfaces.BaseInterfaces;
using Microsoft.EntityFrameworkCore;

namespace BlogBLL.Services
{
    /// <summary>
    /// Service to work with users info
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            _db = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Add role to user
        /// </summary>
        /// <param name="userId">user id that add to role</param>
        /// <param name="role"> role that add to user</param>
        /// <returns><see cref="IdentityResult"/> operation result</returns>
        /// <exception cref="NotFoundException">Throws when user or role not found in db</exception>
        /// <exception cref="BlogException">Throws when Role were empty</exception>
        public async Task<IdentityResult> AddToRole(int userId, string role)
        {
            var user = await _db.UserRepository.GetByIdWithDetailsAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found");
            if (string.IsNullOrEmpty(role))
                throw new BlogException("Role were empty");
            if (!await _db.RoleRepository.RoleExistsAsync(role))
                throw new NotFoundException("Role not found");
            var result = await _db.UserRepository.AddToRoleAsync(user, role);
            await _db.SaveAsync();
            return result;
        }

        /// <summary>
        /// Create user and add to role
        /// </summary>
        /// <param name="user"><see cref="UserDTO"/> user information</param>
        /// <param name="password">Password of the user</param>
        /// <param name="role">Role to which will be added created user</param>
        /// <exception cref="BlogException">Throws when some of parameters in UserDTO were empty or Email or UserName were invalid</exception>
        public async Task CreateUserAndAddToRole(UserDTO user, string password, string role)
        {
            if (string.IsNullOrEmpty(password)
                || string.IsNullOrEmpty(user.FirstName)
                || string.IsNullOrEmpty(user.LastName)
                || string.IsNullOrEmpty(user.UserName)
                || string.IsNullOrEmpty(user.Email))
                throw new BlogException("Some of parameters were empty");

            if (user.FirstName.Contains(" ") || user.LastName.Contains(" "))
                throw new BlogException("FirstName or LastName were contain whitespaces");

            if (password.Contains(" ") || user.Email.Contains(" ") || user.UserName.Contains(" "))
                throw new BlogException("Email, password or username were contain whitespaces");

            if (await _db.UserRepository.GetByEmailAsync(user.Email) != null)
                throw new BlogException("Such email already exist");

            if (await _db.UserRepository.GetAll().FirstOrDefaultAsync(x => x.UserName == user.UserName) != null)
                throw new BlogException("Such username already exist");

            if (string.IsNullOrEmpty(role))
                throw new BlogException("Role were empty");

            if (!await _db.RoleRepository.RoleExistsAsync(role))
                throw new NotFoundException("Role not found");

            var result = await _db.UserRepository.UserAddAsync(_mapper.Map<User>(user), password);

            if (!result.Succeeded)
                throw new BlogException(
                    "Invalid password. Must be at least 6 characters long, have 1 uppercase & 1 lowercase character and special characters");

            await _db.UserRepository.AddToRoleAsync(_mapper.Map<User>(await _db.UserRepository.GetByEmailAsync(user.Email)), role);
            await _db.SaveAsync();
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="id"> user id to delete</param>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task DeleteById(int id)
        {
            if (await _db.UserRepository.GetByIdAsync(id) == null)
                throw new NotFoundException("User not found");
            await _db.UserRepository.DeleteByIdAsync(id);
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns><see cref="IQueryable"/> of <see cref="UserDTO"/></returns>
        public IEnumerable<UserDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_db.UserRepository.GetAll());
        }

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">user email</param>
        /// <returns><see cref="UserDTO"/> info</returns>
        /// <exception cref="BlogException">Throws when Email were empty</exception>
        public async Task<UserDTO> GetByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new BlogException("Email were empty");

            return _mapper.Map<UserDTO>(await _db.UserRepository.GetByEmailAsync(email));
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns><see cref="UserDTO"/> info</returns>
        public async Task<UserDTO> GetById(int id)
        {
            return _mapper.Map<UserDTO>(await _db.UserRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Get user with details by id
        /// </summary>
        /// <param name="id"> user id</param>
        /// <returns><see cref="UserDTO"/> info with details</returns>
        public async Task<UserDTO> GetByIdWithDetails(int id)
        {
            return _mapper.Map<UserDTO>(await _db.UserRepository.GetByIdWithDetailsAsync(id));
        }

        /// <summary>
        /// Update user info
        /// </summary>
        /// <param name="user"><see cref="UserDTO"/> info with new info</param>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        /// <exception cref="BlogException">Throws when some of parameters in UserDTO were empty or Email or UserName were invalid</exception>
        public async Task UpdateUser(UserDTO user)
        {
            if (string.IsNullOrEmpty(user.FirstName)
                || string.IsNullOrEmpty(user.LastName)
                || string.IsNullOrEmpty(user.UserName)
                || string.IsNullOrEmpty(user.Email))
                throw new BlogException("Some of parameters were empty");

            if (user.FirstName.Contains(" ") || user.LastName.Contains(" "))
                throw new BlogException("FirstName or LastName were contain whitespaces");


            if (user.Email.Contains(" ") || user.UserName.Contains(" "))
                throw new BlogException("Email, password or username were contain whitespaces");

            var userFromDb = await _db.UserRepository.GetByIdWithDetailsAsync(user.Id);

            if (userFromDb == null)
                throw new NotFoundException("User not found");

            var userFromDbByEmail = await _db.UserRepository.GetByEmailAsync(user.Email);

            if (userFromDbByEmail != null && userFromDbByEmail.Id != userFromDb.Id)
                throw new BlogException("Such email already exist");

            var userFromDbByUserName = await _db.UserRepository.GetAll().FirstOrDefaultAsync(x => x.UserName == user.UserName);

            if (userFromDbByUserName != null && userFromDbByUserName.Id != userFromDb.Id)
                throw new BlogException("Such username already exist");

            await _db.UserRepository.UpdateAsync(_mapper.Map<User>(user));
            await _db.SaveAsync();
        }

       

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="userId">user id that password should be updated</param>
        /// <param name="currentPassword">current password</param>
        /// <param name="newPassword">new password</param>
        /// <returns><see cref="IdentityResult"/> operation result</returns>
        /// <exception cref="BlogException">Throws when password were invalid</exception>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task<IdentityResult> UserChangePassword(int userId, string currentPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(currentPassword) || string.IsNullOrEmpty(newPassword))
                throw new BlogException("current or new password were empty");

            if (newPassword.Contains(" "))
                throw new BlogException("Password were contain whitespaces");

            var user = await _db.UserRepository.GetByIdAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            var result = await _db.UserRepository.UserChangePasswordAsync(user, currentPassword, newPassword);
            if (!result.Succeeded)
            {
                throw new BlogException("your current password was not correct or new password was invalid. " +
                                            "New password Must be at least 6 characters long, have 1 uppercase & 1 lowercase character and special characters");
            }
            return result;
        }

        /// <summary>
        /// Check user password
        /// </summary>
        /// <param name="userId">user id whose password compared</param>
        /// <param name="password">Password that we compare</param>
        /// <returns>true if passwords matches and false if not</returns>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task<bool> UserCheckPassword(int userId, string password)
        {
            var user = await _db.UserRepository.GetByIdAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found");

            return await _db.UserRepository.UserCheckPasswordAsync(user, password);
        }

        /// <summary>
        /// Gets all roles of user
        /// </summary>
        /// <param name="userId">user id that roles should be returned</param>
        /// <returns><see cref="IEnumerable{T}"/> of roles</returns>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task<IEnumerable<string>> UserGetRoles(int userId)
        {
            var user = await _db.UserRepository.GetByIdAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found");
            return await _db.UserRepository.UserGetRolesAsync(user);
        }
    }
}