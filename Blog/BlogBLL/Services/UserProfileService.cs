﻿using BlogBLL.Interfaces;
using BlogBLL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogDAL.Interfaces.BaseInterfaces;

namespace BlogBLL.Services
{
    /// <summary>
    /// Service to work with user profiles
    /// </summary>
    public class UserProfileService : IUserProfileService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public UserProfileService(IUnitOfWork uow, IMapper mapper)
        {
            _db = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all user profiles with details
        /// </summary>
        /// <returns><see cref="IEnumerable{UserProfileDTO}"/> of user profiles</returns>
        public async Task<IEnumerable<UserProfileDTO>> GetAllUserProfilesWithDetails()
        {
            return _mapper.Map<IEnumerable<UserProfileDTO>>(await _db.UserProfileRepository.GetAllWithDetailsAsync());
        }

        /// <summary>
        /// Get user profile
        /// </summary>
        /// <param name="id"> user id</param>
        /// <returns><see cref="UserProfileDTO"/> user profile info</returns>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task<UserProfileDTO> GetUserProfileById(int id)
        {
            var user = await _db.UserProfileRepository.GetByIdAsync(id);
            if (user == null)
                throw new NotFoundException("User not found");
            return _mapper.Map<UserProfileDTO>(user);
        }

        /// <summary>
        /// Gets user by username
        /// </summary>
        /// <param name="name">user name</param>
        /// <returns><see cref="UserDTO"/></returns>
        public async Task<UserProfileDTO> GetByUserName(string name)
        {
            var user = _db.UserRepository.GetAll().FirstOrDefault(x => x.UserName == name);
            if (user == null)
                throw new NotFoundException("User not found");
            return _mapper.Map<UserProfileDTO>(await _db.UserProfileRepository.GetByIdWithDetailsAsync(user.Id));
        }

        /// <summary>
        /// Get user profile with details by id
        /// </summary>
        /// <param name="id"> user id</param>
        /// <returns><see cref="UserProfileDTO"/> user profile info</returns>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task<UserProfileDTO> GetUserProfileByIdWithDetails(int id)
        {
            var user = await _db.UserProfileRepository.GetByIdWithDetailsAsync(id);
            if (user == null)
                throw new NotFoundException("User not found");
            return _mapper.Map<UserProfileDTO>(user);
        }
    }
}