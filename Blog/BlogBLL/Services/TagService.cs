﻿using BlogBLL.Interfaces;
using BlogBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogBLL.Services
{
    /// <summary>
    /// Service to work with tags
    /// </summary>
    public class TagService : ITagService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public TagService(IUnitOfWork uow, IMapper mapper)
        {
            _db = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Add new tag
        /// </summary>
        /// <param name="genre"><see cref="TagDTO"/> info</param>
        /// <returns>New tag id</returns>
        /// <exception cref="NotFoundException">Throws when tag already exist in db or empty</exception>
        public async Task<int> AddTag(TagDTO tag)
        {
            if (string.IsNullOrEmpty(tag.TagName))
                throw new BlogException("tag were empty");
            if (await (await _db.TagRepository.GetAllAsync()).FirstOrDefaultAsync(x => x.TagName == tag.TagName) != null)
                throw new BlogException("Tag already exist");
            
            await _db.TagRepository.AddAsync(_mapper.Map<Tag>(tag));
            await _db.SaveAsync();
            return tag.Id;
        }

        

        /// <summary>
        /// Delete genre by id
        /// </summary>
        /// <param name="id">Genre id</param>
        /// <exception cref="NotFoundException">Throws when tag not found db</exception>
        public async Task DeleteTagById(int id)
        {
            if (await _db.TagRepository.GetByIdAsync(id) == null)
                throw new NotFoundException("Tag not found");
            _db.TagRepository.DeleteById(id);
            await _db.SaveAsync();
        }

        /// <summary>
        /// All genres
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of genres</returns>
        public async Task<IEnumerable<TagDTO>> GetAllTags()
        {
            return _mapper.Map<IEnumerable<TagDTO>>(await _db.TagRepository.GetAllAsync());
        }

        /// <summary>
        /// Get tag by id
        /// </summary>
        /// <param name="id">tag id</param>
        /// <returns><see cref="TagDTO"/> info</returns>
        /// <exception cref="NotFoundException">Throws when tag not found in db or empty</exception>
        public async Task<TagDTO> GetTagById(int id)
        {
            var tag = await _db.TagRepository.GetByIdAsync(id);
            if (tag == null)
                throw new NotFoundException("Tag not found");
            return _mapper.Map<TagDTO>(tag);
        }

        /// <summary>
        /// Get tag info by tag name
        /// </summary>
        /// <param name="name">Name of tag</param>
        /// <returns><see cref="TagDTO"/> info</returns>
        /// <exception cref="NotFoundException">Throws when tag not found in db or empty</exception>
        public async Task<TagDTO> GetTagByName(string name)
        {
            var tag = await (await _db.TagRepository.GetAllAsync()).FirstOrDefaultAsync(x => x.TagName == name);
            if (tag == null)
                throw new NotFoundException("Tag not found");
            return _mapper.Map<TagDTO>(tag);
        }
    }
}