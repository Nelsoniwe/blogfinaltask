﻿using BlogBLL.Interfaces;
using BlogBLL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogBLL.Exceptions;
using BlogDAL.Interfaces.BaseInterfaces;
using BlogDAL.Models;

namespace BlogBLL.Services
{
    /// <summary>
    /// Service to work with comments
    /// </summary>
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public CommentService(IUnitOfWork uow, IMapper mapper)
        {
            _db = uow;
            _mapper = mapper;
        }
        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="comment"><see cref="CommentDTO"/> new comment info</param>
        /// <returns>New comment id</returns>
        /// <exception cref="BlogException">Throws when comment text were empty</exception>
        /// <exception cref="NotFoundException">Throws when article or user not found in db</exception>
        public async Task<int> AddComment(CommentDTO comment)
        {
            if (string.IsNullOrEmpty(comment.CommentText))
                throw new BlogException("comment text were empty");
            if (await _db.ArticleRepository.GetByIdAsync(comment.ArticleId) == null)
                throw new NotFoundException("Article not found");
            if (await _db.UserProfileRepository.GetByIdAsync(comment.UserId) == null)
                throw new NotFoundException("User not found");

            var commentToDb = _mapper.Map<Comment>(comment);
            await _db.CommentRepository.AddAsync(commentToDb);
            await _db.SaveAsync();
            return commentToDb.Id;
        }
        /// <summary>
        /// Delete comment by id
        /// </summary>
        /// <param name="commentId">comment id</param>
        /// <exception cref="NotFoundException">Throws when comment not found in db</exception>
        public async Task DeleteCommentById(int commentId)
        {
            if (await _db.CommentRepository.GetByIdAsync(commentId) == null)
                throw new NotFoundException("Comment not found");

            _db.CommentRepository.DeleteById(commentId);
            await _db.SaveAsync();
        }
        /// <summary>
        /// Get all article comments by article id
        /// </summary>
        /// <param name="articleId">article id</param>
        /// <returns><see cref="IEnumerable{CommentDTO}"/> CommentDTOs</returns>
        /// <exception cref="NotFoundException">Throws when article not found in db</exception>
        public async Task<IEnumerable<CommentDTO>> GetAllCommentsByArticleId(int articleId)
        {
            var article = await _db.ArticleRepository.GetByIdWithDetailsAsync(articleId);
            if (article == null)
                throw new NotFoundException("Article not found");
            return _mapper.Map<IEnumerable<CommentDTO>>(article.Comments);
        }
        /// <summary>
        /// Get all comments by user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns><see cref="IEnumerable{CommentDTO}"/> CommentDTOs</returns>
        /// <exception cref="NotFoundException">Throws when user not found in db</exception>
        public async Task<IEnumerable<CommentDTO>> GetAllCommentsByUserId(int userId)
        {
            if (await _db.UserProfileRepository.GetByIdAsync(userId) == null)
                throw new NotFoundException("User not found");
            return _mapper.Map<IEnumerable<CommentDTO>>((await _db.CommentRepository.GetAllAsync()).Where(x=>x.UserId==userId));
        }
        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="id">comment id</param>
        /// <returns><see cref="CommentDTO"/> info</returns>
        /// <exception cref="NotFoundException">Throws when comment not found in db</exception>
        public async Task<CommentDTO> GetCommentById(int id)
        {
            var comment = await _db.CommentRepository.GetByIdAsync(id);
            if (comment == null)
                throw new NotFoundException("Comment not found");
            return _mapper.Map<CommentDTO>(comment);
        }
    }
}