﻿using System;
using System.Runtime.Serialization;

namespace BlogBLL.Exceptions
{
    [Serializable]
    public class BlogException : Exception
    {
        public BlogException() { }

        public BlogException(string message) : base(message) { }

        public BlogException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected BlogException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}