﻿using System.Collections.Generic;

namespace BlogBLL.Models
{
    public class BlogDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public ICollection<ArticleDTO> Articles { get; set; }
    }
}