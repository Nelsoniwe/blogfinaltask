﻿using System;
using System.Collections.Generic;
using BlogDAL.Models;

namespace BlogBLL.Models
{
    public class ArticleDTO
    {
        public int Id { get; set; }

        public int BlogId { get; set; }

        public DateTime PublishDate { get; set; }

        public string Body { get; set; }

        public string Title { get; set; }

        public ICollection<TagDTO> Tags { get; set; }

        public ICollection<CommentDTO> Comments { get; set; }
    }
}