﻿using System.Collections.Generic;

namespace BlogBLL.Models
{
    public class TagDTO
    {
        public int Id { get; set; }
        public string TagName { get; set; }
    }
}