﻿namespace BlogBLL.Models
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int UserId { get; set; }
        public string CommentText { get; set; }
    }
}