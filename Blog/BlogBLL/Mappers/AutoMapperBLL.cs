﻿using System.Linq;
using AutoMapper;
using BlogBLL.Models;
using BlogDAL.Models;

namespace BlogBLL.Mappers
{
    public class AutoMapperBLL : Profile
    {
        public AutoMapperBLL()
        {
            CreateMap<CommentDTO, Comment>().ReverseMap();

            CreateMap<User, UserDTO>()
                .ForMember(p => p.FirstName, c => c.MapFrom(src => src.UserProfile.FirstName))
                .ForMember(p => p.LastName, c => c.MapFrom(src => src.UserProfile.LastName))
                .ReverseMap();

            CreateMap<Tag, TagDTO>()
                .ReverseMap();

            CreateMap<Comment, CommentDTO>()
                .ReverseMap();

            CreateMap<Article, ArticleDTO>()
                .ForMember(p => p.Tags, c => c.MapFrom(src => src.Tags))
                .ReverseMap();

            CreateMap<UserProfile, UserProfileDTO>()
                .ForMember(p => p.Email, c => c.MapFrom(src => src.AppUser.Email))
                .ForMember(p => p.UserName, c => c.MapFrom(src => src.AppUser.UserName))
                .ForMember(p=>p.Blog,c=>c.MapFrom(src=>src.Blog))
                .ReverseMap();

            CreateMap<Blog, BlogDTO>()
                .ReverseMap();
            CreateMap<UserProfileDTO, UserDTO>().ReverseMap();
        }
    }
}