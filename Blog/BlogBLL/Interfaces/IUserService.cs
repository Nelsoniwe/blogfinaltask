﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogBLL.Models;
using BlogDAL.Models;
using Microsoft.AspNetCore.Identity;

namespace BlogBLL.Interfaces
{
    /// <summary>
    /// Service interface to work with users info
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns><see cref="IQueryable"/> of <see cref="UserDTO"/></returns>
        IEnumerable<UserDTO> GetAll();

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns><see cref="UserDTO"/> info</returns>
        Task<UserDTO> GetById(int id);

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">user email</param>
        /// <returns><see cref="UserDTO"/> info</returns>
        Task<UserDTO> GetByEmail(string email);

        /// <summary>
        /// Check user password
        /// </summary>
        /// <param name="userId">user id whose password compared</param>
        /// <param name="password">Password that we compare</param>
        /// <returns>true if passwords matches and false if not</returns>
        Task<bool> UserCheckPassword(int userId, string password);

        /// <summary>
        /// Create user and add to role
        /// </summary>
        /// <param name="user"><see cref="UserDTO"/> user information</param>
        /// <param name="password">Password of the user</param>
        /// <param name="role">Role to which will be added created user</param>
        Task CreateUserAndAddToRole(UserDTO user, string password, string role);

        /// <summary>
        /// Update user info
        /// </summary>
        /// <param name="user"><see cref="UserDTO"/> info with new info</param>
        Task UpdateUser(UserDTO user);

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="id"> user id to delete</param>
        Task DeleteById(int id);

        /// <summary>
        /// Get user with details by id
        /// </summary>
        /// <param name="id"> user id</param>
        /// <returns><see cref="UserDTO"/> info with details</returns>
        Task<UserDTO> GetByIdWithDetails(int id);

        /// <summary>
        /// Add role to user
        /// </summary>
        /// <param name="userId">user id that add to role</param>
        /// <param name="role"> role that add to user</param>
        /// <returns><see cref="IdentityResult"/> operation result</returns>
        Task<IdentityResult> AddToRole(int userId, string role);

        /// <summary>
        /// Gets all roles of user
        /// </summary>
        /// <param name="userId">user id that roles should be returned</param>
        /// <returns><see cref="IEnumerable{T}"/> of roles</returns>
        Task<IEnumerable<string>> UserGetRoles(int userId);


        

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="userId">user id that password should be updated</param>
        /// <param name="currentPassword">current password</param>
        /// <param name="newPassword">new password</param>
        /// <returns><see cref="IdentityResult"/> operation result</returns>
        Task<IdentityResult> UserChangePassword(int userId, string currentPassword, string newPassword);
    }
}