﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlogBLL.Models;

namespace BlogBLL.Interfaces
{
    /// <summary>
    /// Service interface to work with blogs
    /// </summary>
    public interface IBlogService
    {
        /// <summary>
        /// Create blog
        /// </summary>
        /// <param name="userId">user id whose blog to create</param>
        /// <returns>New blog id</returns>
        Task<int> CreateBlog(int userId);

        /// <summary>
        /// Delete blog by id
        /// </summary>
        /// <param name="blogId">blog id</param>
        Task DeleteBlogById(int blogId);

        /// <summary>
        /// Get all blogs with details
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> BlogDTOs</returns>
        Task<IEnumerable<BlogDTO>> GetAllBlogsWithDetails();

        /// <summary>
        /// Get blog with details
        /// </summary>
        /// <param name="id">blog id</param>
        /// <returns><see cref="BlogDTO"/> info</returns>
        Task<BlogDTO> GetBlogByIdWithDetails(int id);

        /// <summary>
        /// Get blog by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns><see cref="BlogDTO"/></returns>
        Task<BlogDTO> GetBlogByUserName(string userName);

        /// <summary>
        /// Get blog by user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns><see cref="BlogDTO"/></returns>
        Task<BlogDTO> GetBlogByUserId(int userId);

        /// <summary>
        /// Get all blogs 
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> BlogDTOs</returns>
        public Task<IEnumerable<BlogDTO>> GetAllBlogs();

        /// <summary>
        /// Get blog by id
        /// </summary>
        /// <param name="userId">blog id</param>
        /// <returns><see cref="BlogDTO"/></returns>
        Task<BlogDTO> GetBlogById(int blogId);
    }
}