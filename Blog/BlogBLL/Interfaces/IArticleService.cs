﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlogBLL.Models;

namespace BlogBLL.Interfaces
{
    /// <summary>
    /// Service interface to work with articles
    /// </summary>
    public interface IArticleService
    {
        /// <summary>
        /// Add new article
        /// </summary>
        /// <param name="article"><see cref="ArticleDTO"/> new article info</param>
        /// <returns>New article id</returns>
        Task<int> AddArticle(ArticleDTO article);

        /// <summary>
        /// Delete article by id
        /// </summary>
        /// <param name="articleId">article id</param>
        Task DeleteArticleById(int articleId);

        /// <summary>
        /// Get all articles with details
        /// </summary>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> Articles</returns>
        Task<IEnumerable<ArticleDTO>> GetAllArticlesWithDetails();

        /// <summary>
        /// Get all articles
        /// </summary>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> Articles</returns>
        Task<IEnumerable<ArticleDTO>> GetAllArticles();

        /// <summary>
        /// Get article by id with details
        /// </summary>
        /// <param name="id">article id</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        Task<ArticleDTO> GetArticleByIdWithDetails(int id);

        /// <summary>
        /// Update Article info
        /// </summary>
        /// <param name="article">article with id and new info</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        Task UpdateArticle(ArticleDTO article);

        /// <summary>
        /// Add new tag to Article
        /// </summary>
        /// <param name="articleId">article id where we want to add tag in</param>
        /// <param name="tag">tag name</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        Task AddTag(int articleId,string tag);

        /// <summary>
        /// Get article by id
        /// </summary>
        /// <param name="id">article id</param>
        /// <returns><see cref="ArticleDTO"/> info</returns>
        Task<ArticleDTO> GetArticleById(int id);

        /// <summary>
        /// Get all articles by user name
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticlesDTOs</returns>
        Task<IEnumerable<ArticleDTO>> GetAllArticlesByUserName(string userName);

        /// <summary>
        /// Get all articles by user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticlesDTOs</returns>
        Task<IEnumerable<ArticleDTO>> GetAllArticlesByUserId(int userId);

        /// <summary>
        /// Get all articles by tag
        /// </summary>
        /// <param name="tag">tag </param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticleDTOs</returns>
        Task<IEnumerable<ArticleDTO>> GetAllArticlesByTag(string tagName);

        /// <summary>
        /// Get all articles by text in body
        /// </summary>
        /// <param name="tag">tag </param>
        /// <returns><see cref="IEnumerable{ArticleDTO}"/> ArticleDTOs</returns>
        Task<IEnumerable<ArticleDTO>> GetAllArticlesByText(string text);

    }
}