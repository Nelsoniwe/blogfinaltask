﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlogBLL.Models;

namespace BlogBLL.Interfaces
{
    /// <summary>
    /// Service interface to work with tags
    /// </summary>
    public interface ITagService
    {
        /// <summary>
        /// Add new tag
        /// </summary>
        /// <param name="genre"><see cref="TagDTO"/> info</param>
        /// <returns>New tag id</returns>
        Task<int> AddTag(TagDTO tag);

        /// <summary>
        /// All genres
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of genres</returns>
        Task<IEnumerable<TagDTO>> GetAllTags();

        /// <summary>
        /// Get tag by id
        /// </summary>
        /// <param name="id">tag id</param>
        /// <returns><see cref="TagDTO"/> info</returns>
        Task<TagDTO> GetTagById(int id);

        /// <summary>
        /// Delete genre by id
        /// </summary>
        /// <param name="id">Genre id</param>
        Task DeleteTagById(int id);

        /// <summary>
        /// Get tag info by tag name
        /// </summary>
        /// <param name="name">Name of tag</param>
        /// <returns><see cref="TagDTO"/> info</returns>
        Task<TagDTO> GetTagByName(string name);
    }
}