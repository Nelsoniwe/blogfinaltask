﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlogBLL.Models;

namespace BlogBLL.Interfaces
{
    /// <summary>
    /// Service interface to work with comments
    /// </summary>
    public interface ICommentService
    {
        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="comment"><see cref="CommentDTO"/> new comment info</param>
        /// <returns>New comment id</returns>
        Task<int> AddComment(CommentDTO comment);

        /// <summary>
        /// Delete comment by id
        /// </summary>
        /// <param name="commentId">comment id</param>
        Task DeleteCommentById(int commentId);

        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="id">comment id</param>
        /// <returns><see cref="CommentDTO"/> info</returns>
        Task<CommentDTO> GetCommentById(int id);

        /// <summary>
        /// Get all comments by user id
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns><see cref="IEnumerable{CommentDTO}"/> CommentDTOs</returns>
        Task<IEnumerable<CommentDTO>> GetAllCommentsByUserId(int userId);

        /// <summary>
        /// Get all article comments by article id
        /// </summary>
        /// <param name="articleId">article id</param>
        /// <returns><see cref="IEnumerable{CommentDTO}"/> CommentDTOs</returns>
        Task<IEnumerable<CommentDTO>> GetAllCommentsByArticleId(int articleId);
    }
}