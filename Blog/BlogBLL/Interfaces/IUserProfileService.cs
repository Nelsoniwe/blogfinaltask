﻿using BlogBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogBLL.Interfaces
{
    /// <summary>
    /// Service interface to work with user profiles
    /// </summary>
    public interface IUserProfileService
    {
        /// <summary>
        /// Get all user profiles with details
        /// </summary>
        /// <returns><see cref="IEnumerable{UserProfileDTO}"/> of user profiles</returns>
        Task<IEnumerable<UserProfileDTO>> GetAllUserProfilesWithDetails();

        /// <summary>
        /// Get user profile with details by id
        /// </summary>
        /// <param name="id"> user id</param>
        /// <returns><see cref="UserProfileDTO"/> user profile info</returns>
        Task<UserProfileDTO> GetUserProfileByIdWithDetails(int id);

        /// <summary>
        /// Gets user by username
        /// </summary>
        /// <param name="name">user name</param>
        /// <returns><see cref="UserDTO"/></returns>
        Task<UserProfileDTO> GetByUserName(string name);

        /// <summary>
        /// Get user profile
        /// </summary>
        /// <param name="id"> user id</param>
        /// <returns><see cref="UserProfileDTO"/> user profile info</returns>
        Task<UserProfileDTO> GetUserProfileById(int id);
    }
}