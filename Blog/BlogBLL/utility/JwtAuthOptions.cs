﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace BlogBLL.utility
{
    /// <summary>
    /// Jwt authentication options
    /// </summary>
    public static class JwtAuthOptions
    {
        public const string Audience = "http://localhost:4200/";
        public const string Issuer = "https://localhost:44354";
        private const string Key = "v890zyhy301y2hjgbiuftg97vgfqw9j0ycash9qb23rkjh";
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}