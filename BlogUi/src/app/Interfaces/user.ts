import { Blog } from "./blog";

export interface User {
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    blog? : Blog;
}
