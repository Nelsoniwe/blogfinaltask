import { Tag } from "./tag";

export interface ArticleAdd {
    body: string;
    title: string;
    tags : Tag[];
}