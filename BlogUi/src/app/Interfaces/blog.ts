import { Article } from "./article";

export interface Blog {
    id ?: number;
    userId ?: number;
    articles ?: Article[]
}
