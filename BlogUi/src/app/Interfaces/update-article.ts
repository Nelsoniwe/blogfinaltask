import { Tag } from "./tag";

export interface ArticleUpdate {
    id:number;
    body: string;
    title: string;
    tags : Tag[];
}