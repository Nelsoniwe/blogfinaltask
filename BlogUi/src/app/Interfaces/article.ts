import { comment } from "./comment";
import { Tag } from "./tag";

export interface Article {
    id: number;
    blogId: number;
    publishDate: Date;
    body: string;
    title: string;
    tags: Tag[];
    comments : comment[];
}