import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Blog } from 'src/app/Interfaces/blog';

import { BLOG_API_URL } from 'src/app/models/app-injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient, 
    @Inject(BLOG_API_URL) private apiUrl: string) { }
  
  userBlogExist(userId:number):boolean{
    var response;
     this.http.get<Blog>(`${this.apiUrl}/api/Blog/GetByUserId?id=${userId}`,{observe:'response'}).subscribe(data => response = data.status);
    if (response == 200) {
      return true;
    }
    else return false;
  }

  getBlogByUserId(id : number):Observable<Blog>{
    return this.http.get<Blog>(`${this.apiUrl}/api/Blog/GetByUserId?id=${id}`);
  }

  getBlogById(id : number):Observable<Blog>{
    return this.http.get<Blog>(`${this.apiUrl}/api/Blog/GetById?id=${id}`);
  }

  deleteBlog(id:number){
    return this.http.delete<Blog>(`${this.apiUrl}/api/Blog?id=${id}`);
  }

  createBlog(){
    return this.http.post<Blog>(`${this.apiUrl}/api/Blog`,{});
  }
}

