import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { comment } from 'src/app/Interfaces/comment'; 
import { commentAdd } from 'src/app/Interfaces/commentAdd';

import { BLOG_API_URL } from 'src/app/models/app-injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient, 
    @Inject(BLOG_API_URL) private apiUrl: string) { }
  
  getCommentById(id : number):Observable<comment>{
    return this.http.get<comment>(`${this.apiUrl}/api/Comment?id=${id}`);
  }

  getCommentsByArticleId(id : number):Observable<comment[]>{
    return this.http.get<comment[]>(`${this.apiUrl}/api/Comment/GetByArticleId?id=${id}`);
  }

  getCommentsByUserId(id : number):Observable<comment[]>{
    return this.http.get<comment[]>(`${this.apiUrl}/api/Comment/GetByUserId?id=${id}`);
  }

  addComment(commentToAdd: commentAdd):Observable<number>{
    return this.http.post<number>(`${this.apiUrl}/api/Comment`, commentToAdd);
  }

  deleteComment(id : number){
    return this.http.delete<number>(`${this.apiUrl}/api/Comment?id=${id}`);
  }
}