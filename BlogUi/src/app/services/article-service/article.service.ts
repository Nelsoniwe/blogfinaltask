import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BLOG_API_URL  } from 'src/app/models/app-injection-tokens';
import { Observable } from 'rxjs';
import { Article } from 'src/app/Interfaces/article';
import { ArticleAdd } from 'src/app/Interfaces/article-add';
import { ArticleUpdate } from 'src/app/Interfaces/update-article';

@Injectable({
    providedIn: 'root'
  })
export class ArticleService{

    constructor(private http: HttpClient, 
        @Inject(BLOG_API_URL) private apiUrl: string) { }


    getAllArticles():Observable<Article[]>{
        return this.http.get<Article[]>(`${this.apiUrl}/api/Article/GetAll`);
    }   

    getArticleById(id:number):Observable<Article>{
        return this.http.get<Article>(`${this.apiUrl}/api/Article/GetById?id=${id}`);
    }

    getArticlesByText(text:string):Observable<Article[]>{
        let params = new HttpParams().set('text', text);
        console.log(text)
        return this.http.get<Article[]>(`${this.apiUrl}/api/Article/GetByText`,{params:params});
    }

    getArticlesByTag(tag:string):Observable<Article[]>{
        let params = new HttpParams().set('tag', tag);
        console.log(tag)
        return this.http.get<Article[]>(`${this.apiUrl}/api/Article/GetByTag`,{params:params});
    }

    deleteArticle(id:number){
        return this.http.delete<number>(`${this.apiUrl}/api/Article?id=${id}`);
    }

    createArticle(article:ArticleAdd){
        return this.http.post<number>(`${this.apiUrl}/api/Article`,article);
    }

    updateArticle(article:ArticleUpdate){
        return this.http.put<number>(`${this.apiUrl}/api/Article`,article);
    }
}