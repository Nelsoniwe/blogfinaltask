import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterseptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let userToken = this.auth.getUserToken();
    if(userToken !== ''){
        request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${userToken}`
            }
          });
    }
    return next.handle(request);
  }
}