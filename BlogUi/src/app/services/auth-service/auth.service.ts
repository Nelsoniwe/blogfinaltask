import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { Token } from 'src/app/Interfaces/token';
import { BLOG_API_URL } from 'src/app/models/app-injection-tokens';
import { tap } from 'rxjs/operators';
import { Register } from 'src/app/Interfaces/register';

export const ACCESS_TOKEN_KEY = 'blogui_access_token';
export const USER_ID = 'user_id';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    @Inject(BLOG_API_URL) private apiUrl: string,
    private jwtHelper: JwtHelperService
  ) { }

  login(email: string, password: string): Observable<Token> {
    return this.http.post<Token>(`${this.apiUrl}/api/Auth/login`, {
      email, password
    }).pipe(
      tap(userToken => {
        localStorage.setItem(ACCESS_TOKEN_KEY, userToken.userToken);
        localStorage.setItem(USER_ID, userToken.userId);
      })
    )
  }

  isAuthenticated(): boolean {
    var token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if(token){
      return !this.jwtHelper.isTokenExpired(token)
    }
    return false;
  }

  getUserId(): string {
    var id = localStorage.getItem(USER_ID);
    if(id){
      return id;
    }
    return '';
  }

  getUserToken(): string {
    var token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if(token){
      return token;
    }
    return '';
  }

  logout(): void{
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    localStorage.removeItem(USER_ID);
  }

  register(userInfo : Register){
    return this.http.post(`${this.apiUrl}/api/Auth/register`, userInfo);
  }

 
}
