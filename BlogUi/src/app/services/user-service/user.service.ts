import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChangePassword } from 'src/app/Interfaces/change-password';
import { updateUser } from 'src/app/Interfaces/update-user';
import { User } from 'src/app/Interfaces/user';
import { BLOG_API_URL } from 'src/app/models/app-injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  

  constructor(private http: HttpClient, 
    @Inject(BLOG_API_URL) private apiUrl: string) { }

  getAllUserProfiles():Observable<User[]>{
      return this.http.get<User[]>(`${this.apiUrl}/api/User/GetAllUserProfiles`);
  }
  
  getUserById(id : number):Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/api/User/GetProfile/${id}`);
  }

  getRoles():Observable<string[]>{
    return this.http.get<string[]>(`${this.apiUrl}/api/User/GetRoles`);
  }

  getProfile():Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/api/User/GetProfile`);
  }

  addUserToRole(userId: number, roleName: string):Observable<any>{
    return this.http.post(`${this.apiUrl}/api/User/AddToRole/${userId}/${roleName}`, null);
  }

  deleteUser():Observable<any>{
    return this.http.delete(`${this.apiUrl}/api/User/Delete`);
  }

  deleteUserById(userId: number):Observable<any>{
    return this.http.delete(`${this.apiUrl}/api/User/Delete/${userId}`);
  }

  updateProfile(user: updateUser) {
    return this.http.put(`${this.apiUrl}/api/User/UpdateUser/`,user);
  }

  updateProfileById(id:number,user: updateUser) {
    return this.http.put(`${this.apiUrl}/api/User/UpdateUser/${id}`,user);
  }

  changePassword(changePassword:ChangePassword){
    return this.http.put(`${this.apiUrl}/api/User/ChangePassword`, changePassword);
  }
}
