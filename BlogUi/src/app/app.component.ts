import { Component,ViewEncapsulation  } from '@angular/core';

import { ArticleService } from './services/article-service/article.service';
import { AuthService } from './services/auth-service/auth.service';
import { Article } from './Interfaces/article';

import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent{

  constructor(private authService:AuthService,private articleService: ArticleService, private router: Router) { }

  title = 'BlogUi';

  articles: Article[] = [];

  public get isLoggedIn() : boolean {
    return this.authService.isAuthenticated();
  }

  public get thisUserId() : string {
    return this.authService.getUserId();
  }

  search(){
    this.articleService.getAllArticles().subscribe((data) => 
    {
      this.articles = data;
      
    })
    return this.articles;
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['']).then(() => {
      window.location.reload();
    });
  }
  
}