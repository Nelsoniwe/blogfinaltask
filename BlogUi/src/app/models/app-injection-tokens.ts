import { InjectionToken } from '@angular/core'

export const BLOG_API_URL = new InjectionToken<string>('https://localhost:44354');