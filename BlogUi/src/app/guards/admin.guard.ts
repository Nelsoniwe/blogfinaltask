import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from '../services/user-service/user.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private userSerivce: UserService,  private router: Router) {}

  canActivate() : boolean{
    this.userSerivce.getRoles().subscribe(roles => {
      if(!roles.includes("Admin")){
        this.router.navigate([''])
      }
    });
    return true;
  }
}
