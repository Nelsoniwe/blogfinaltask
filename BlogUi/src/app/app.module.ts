import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';

import { Article } from './Interfaces/article';
import { BLOG_API_URL } from './models/app-injection-tokens';
import { ArticleItemComponent } from './components/article/article-item/article-item.component';
import { CommentItemComponent } from './components/comment/comment-item.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import { LoginComponent } from './components/login/login.component';
import { ACCESS_TOKEN_KEY } from './services/auth-service/auth.service';
import { JwtModule } from '@auth0/angular-jwt';
import { HomeComponent } from './components/home/home.component';
import { JwtInterseptor } from './services/auth-service/jwt-interseptor';
import { BlogComponent } from './components/blog/blog.component';
import { RegisterComponent } from './components/register/register.component';
import { ArticleAddComponent } from './components/article/article-add/article-add.component';
import { ProfileComponent } from './components/profile/profile.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { ArticleUpdateComponent } from './components/article/article-update/article-update.component';
import { SearchComponent } from './components/search/search.component';
import { ChangePasswordComponent } from './components/changePassword/change-password.component';

export function tokenGetter() {
  return localStorage.getItem(ACCESS_TOKEN_KEY);
}

@NgModule({
  declarations: [
    BlogComponent,
    HomeComponent,
    AppComponent,
    ArticleItemComponent,
    CommentItemComponent,
    LoginComponent,
    RegisterComponent,
    ArticleAddComponent,
    ProfileComponent,
    ArticleUpdateComponent,
    SearchComponent,
    ChangePasswordComponent
  ],
  imports: [
    
    MatSliderModule,
    MatMenuModule,
    MatGridListModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatSelectModule,
    MatDividerModule,
    MatListModule,
    

    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatInputModule,

    JwtModule.forRoot({
      config: {
        tokenGetter,
        allowedDomains: [environment.tokenWhiteListedDomains]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterseptor,
      multi: true
    },
    {
      provide: BLOG_API_URL,
      useValue: environment.BlogApi
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
