import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { RegisterComponent } from './components/register/register.component'; 
import { ArticleAddComponent } from './components/article/article-add/article-add.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ArticleUpdateComponent } from './components/article/article-update/article-update.component';
import { SearchComponent } from './components/search/search.component';
import { ChangePasswordComponent } from './components/changePassword/change-password.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "blog/:id", component: BlogComponent},
  {path: "login", component: LoginComponent, canActivate: [IsNotLoggedGuard]},
  {path: "register", component: RegisterComponent, canActivate: [IsNotLoggedGuard]},
  {path: "addArticle", component: ArticleAddComponent, canActivate: [IsLoggedGuard]},
  {path: "profile/:id", component: ProfileComponent},
  {path: "updateArticle/:id", component: ArticleUpdateComponent, canActivate: [IsLoggedGuard]},
  {path: "search", component: SearchComponent},
  {path: "changePassword", component: ChangePasswordComponent,canActivate: [IsLoggedGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
