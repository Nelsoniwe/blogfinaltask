import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Article } from 'src/app/Interfaces/article';
import { comment } from 'src/app/Interfaces/comment';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
    selector: 'app-comment-item',
    templateUrl: './comment-item.component.html',
    styleUrls: ['./comment-item.component.css']
  })
export class CommentItemComponent implements OnInit{
    
    constructor(public userService : UserService,private commentService:CommentService,private authService:AuthService) {}

    ngOnInit(): void {
      this.userService.getUserById(this.item.userId).subscribe((data) => this.userName = data.userName);
      if (this.authService.isAuthenticated()) {
        this.userService.getRoles().subscribe(data=> this.isAdmin = data.includes("Admin"))
        this.userService.getProfile().subscribe(data=> this.isCommentOwned = data.id == this.item.userId.toString());
      }
    }

    @Input() item!: comment; 

    id!: number;
    articleId!: number;
    userId!: number;
    commentText!: string;
  
    userName! : string;

    isAdmin! : Boolean;
    isCommentOwned! : boolean;

    get canDelete() : boolean{

      if (this.authService.isAuthenticated() && (this.isAdmin || this.isCommentOwned)) {
        return true;
      }
      return false;
    }

    deleteComment():void{
        this.commentService.deleteComment(this.item.id).subscribe();
    }
}
