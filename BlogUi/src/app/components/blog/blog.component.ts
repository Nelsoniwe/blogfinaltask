import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Blog } from 'src/app/Interfaces/blog';
import { Article } from 'src/app/Interfaces/article';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { BlogService } from 'src/app/services/blog-service/blog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Interfaces/user';
import { UserService } from 'src/app/services/user-service/user.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
    
  blog: Blog = {userId:0,id:0};
  user:User=  {
      id: '',
      firstName: '',
      lastName: '',
      userName: '',
      email: '',
      blog! : {}
  };
  roles:string[]=[]

  
  constructor(private router: Router,private route: ActivatedRoute, private blogService: BlogService,private userService:UserService,private authService:AuthService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }
    ngOnInit(): void {  
        this.blogService.getBlogByUserId(this.route.snapshot.params['id']).subscribe(data => {
            this.blog.articles = data.articles?.reverse();
            this.blog.id = data.id;
            this.blog.userId = data.userId});
        this.userService.getUserById(this.route.snapshot.params['id']).subscribe(data => {var a = data; this.user = a;});
        this.userService.getRoles().subscribe(data=>this.roles=data)
    }

  searchOrders: string[] = ["Ascending", "Descending"]

  searchForm!: FormGroup;

  get canDeleteBlog():boolean{
    if (this.authService.getUserId() == this.blog.userId?.toString() || this.roles.includes("Admin")) {
      return true;
    }
    return false;
  }

  get canCreateBlog():boolean{
    if (this.authService.getUserId() == this.blog.userId?.toString() && this.blog.id!=0) {
      return true;
    }
    return false;
  }

  deleteBlog(){
    if(confirm("Are you sure to delete blog") && this.blog.id != undefined) {
      this.blogService.deleteBlog(this.blog.id).subscribe();
      window.location.reload();
    }
  }

  createBlog(){
    this.blogService.createBlog().subscribe();
    window.location.reload();
  }

  createArticle(){

  }

}
