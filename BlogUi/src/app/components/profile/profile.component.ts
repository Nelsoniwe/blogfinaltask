import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Blog } from 'src/app/Interfaces/blog';
import { Article } from 'src/app/Interfaces/article';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { BlogService } from 'src/app/services/blog-service/blog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Interfaces/user';
import { UserService } from 'src/app/services/user-service/user.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { ApiError } from 'src/app/Interfaces/api-error';

@Component({
  selector: 'app-blog',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user:User=  {
      id: '',
      firstName: '',
      lastName: '',
      userName: '',
      email: '',
      blog! : {}
  };
  roles:string[]=[]
  updateForm!: FormGroup;
  updateError!: string;


  constructor(private router: Router,private route: ActivatedRoute, private blogService: BlogService,private userService:UserService,private authService:AuthService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }
    ngOnInit(): void {  
        this.userService.getUserById(this.route.snapshot.params['id']).subscribe(data => {var a = data; this.user = a;});
        this.userService.getRoles().subscribe(data=>this.roles=data)

        this.userService.getProfile().subscribe((profile) => {
          this.user.firstName = profile.firstName;
          this.user.lastName = profile.lastName;
          this.updateForm.setValue({
            firstName: profile.firstName,
            lastName: profile.lastName,
            userName: profile.userName,
            email: profile.email
          });
        });
    
        this.updateError = '';
        this.updateForm = new FormGroup({
          firstName: new FormControl(),
          lastName: new FormControl(),
          userName: new FormControl(),
          email: new FormControl(),
        });
    }

  searchOrders: string[] = ["Ascending", "Descending"]
  searchForm!: FormGroup;

  get canDelete():boolean{
    if (this.authService.getUserId() == this.user.id?.toString() || this.roles.includes("Admin")) {
      return true;
    }
    return false;
  }

  get canChangeProfile():boolean{
    if (this.authService.getUserId() == this.user.id?.toString()) {
      return true;
    }
    return false;
  }

  get canDeleteProfile():boolean{
    if (this.authService.getUserId() == this.user.id?.toString() || this.roles.includes("Admin")) {
      return true;
    }
    return false;
  }

  deleteUser(){
    if(confirm("Are you sure to delete user")) {
      if (this.user.id == this.authService.getUserId()) {
        this.userService.deleteUser().subscribe();
        this.logout();
      }
      else{
        this.userService.deleteUserById(parseInt(this.user.id)).subscribe();
        this.router.navigate(['']).then(() => {
          window.location.reload();
        });
      }
      
    }
  }

  createBlog(){
    this.blogService.createBlog().subscribe();
    window.location.reload();
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['']).then(() => {
      window.location.reload();
    });
  }

  update(){
    if (this.authService.getUserId() == this.user.id) {
      this.userService.updateProfile(this.updateForm.value).subscribe(
        () => {
          alert("Update successfully completed!");
        },
        (exc) => {
          this.updateError = ApiError.StringBuilder(exc);
        });
    }
    else
    {
      this.userService.updateProfileById(parseInt(this.user.id),this.updateForm.value).subscribe(
        () => {
          alert("Update successfully completed!");
        },
        (exc) => {
          this.updateError = ApiError.StringBuilder(exc);
        });
    }
    window.location.reload();
  }
}
