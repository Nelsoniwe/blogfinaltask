import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Blog } from 'src/app/Interfaces/blog';
import { Article } from 'src/app/Interfaces/article';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  constructor(private router: Router,private articleService: ArticleService) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

    ngOnInit(): void {
        this.articleService.getAllArticles().subscribe((data) => 
        {
          this.articles = data?.reverse();
        });
        this.searchForm = new FormGroup({
            search: new FormControl(),
            tags: new FormControl(),
            sortOrder: new FormControl()
          });
      }
    

  articles: Article[] = [];

  searchOrders: string[] = ["Ascending", "Descending"]

  searchForm!: FormGroup;


  search(){
    let searchConditions = this.searchForm.value;
    console.log(searchConditions);
    this.articleService.getAllArticles().subscribe((data) => {
      this.articles = data;
      if(searchConditions.search){
        this.articles = this.articles.filter(text => {
          return text.title.toLowerCase().includes((searchConditions.search + "").toLowerCase());
        });
      }
    });
  }
}
