import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Blog } from 'src/app/Interfaces/blog';
import { Article } from 'src/app/Interfaces/article';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { BlogService } from 'src/app/services/blog-service/blog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Interfaces/user';
import { UserService } from 'src/app/services/user-service/user.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { ApiError } from 'src/app/Interfaces/api-error';

@Component({
  selector: 'app-blog',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    
  constructor(private router: Router,private articleService:ArticleService,private route: ActivatedRoute, private blogService: BlogService,private userService:UserService,private authService:AuthService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }
    ngOnInit(): void {  
      this.articles.push()
      // this.articleService.getArticleById(2).subscribe(data=>this.articles.push(data));
    }
    articles:Article[]=[];
    text:string = "";
    tag:string = "";
    searchError: string ="";

    getByText(){
      this.articles = []
      this.articleService.getArticlesByText(this.text).subscribe(data=>{
        this.articles = data;
        this.searchError = '' ;
      },(exc) => {
        this.searchError = ApiError.StringBuilder(exc);
        console.log(exc);
      });
    }

    getByTag(){
      this.articles = []
      this.articleService.getArticlesByTag(this.tag).subscribe(data=>{
        this.articles = data;
        this.searchError = '' ;
      },(exc) => {
        this.searchError = ApiError.StringBuilder(exc);
        console.log(exc);
      });
    }
}
