import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {}
  
  loginForm!: FormGroup;

  loginError!: string;
  
  ngOnInit() {
    this.loginError = '';
    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    });
  }

  login(){
    this.authService.login(this.loginForm.controls['email'].value, this.loginForm.controls['password'].value)
      .subscribe(() => {
        this.router.navigate(['']).then(() => {
          window.location.reload();
        });
      }, () => {
        this.loginError = "Invalid login or password";
      });
  }
}
