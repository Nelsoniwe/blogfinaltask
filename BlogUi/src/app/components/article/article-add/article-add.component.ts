import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiError } from 'src/app/Interfaces/api-error';
import { ArticleAdd } from 'src/app/Interfaces/article-add';
import { Tag } from 'src/app/Interfaces/tag';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.css']
})
export class ArticleAddComponent implements OnInit {

  constructor(private router: Router,private articleService: ArticleService,private authService:AuthService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
    return false;
  };}
  
  tags:Tag[]=[]
  userId : string = '';
  addArticleForm!: FormGroup;
  articleError!: string;
  ngOnInit() {
    this.articleError = '';
    this.userId = this.authService.getUserId();
    this.addArticleForm = new FormGroup({
      title: new FormControl(),
      body: new FormControl()
    });
  }

  deleteTag(){
    this.tags.pop();
  }

  addTag(){
    this.tags.push({tagName : ""});
    console.log(this.tags)
  }
  addArticle(){
    var addArticle : ArticleAdd = {
      body: this.addArticleForm.value.body,
      title: this.addArticleForm.value.title,
      tags: this.tags
    }
    this.articleService.createArticle(addArticle).subscribe(() => {
      this.router.navigate([`/blog/${this.userId}`]).then(() => {
        window.location.reload();
      });
    }, (exc) => {
      this.articleError = ApiError.StringBuilder(exc);
    });
  }
}
