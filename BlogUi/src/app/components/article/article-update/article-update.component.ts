import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleAdd } from 'src/app/Interfaces/article-add';
import { Tag } from 'src/app/Interfaces/tag';
import { ArticleUpdate } from 'src/app/Interfaces/update-article';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './article-update.component.html',
  styleUrls: ['./article-update.component.css']
})
export class ArticleUpdateComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute,private articleService: ArticleService,private authService:AuthService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
    return false;
  };}
  
  tags:Tag[]=[]
  userId : string = '';
  updateArticleForm!: FormGroup;
  loginError!: string;
  article:ArticleUpdate = {
    id: 0,
    body: '',
    title: '',
    tags: []
  };

  ngOnInit() {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(data => {
      this.article.id = data.id;
      this.article.title = data.title;
      this.article.body = data.body;
      this.tags = data.tags;
      
      
      this.updateArticleForm.setValue({
        title: data.title,
        body: data.body,
      });
    });
    this.loginError = '';
    this.userId = this.authService.getUserId();

    this.updateArticleForm = new FormGroup({
      title: new FormControl(),
      body: new FormControl()
    });
  }

  deleteTag(){
    this.tags.pop();
  }

  addTag(){
    this.tags.push({tagName : ""});
    console.log(this.article)
  }
  updateArtice(){
    var updateArticle : ArticleUpdate = {
      id: this.article.id,
      body: this.updateArticleForm.value.body,
      title: this.updateArticleForm.value.title,
      tags: this.tags
    }
    this.articleService.updateArticle(updateArticle).subscribe(() => {
      this.router.navigate([`/blog/${this.userId}`]).then(() => {
        window.location.reload();
      });
    }, () => {
      this.loginError = "Invalid login or password";
    });
  }
}
