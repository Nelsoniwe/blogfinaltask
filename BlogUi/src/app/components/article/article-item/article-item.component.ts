import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { waitForAsync } from '@angular/core/testing';
import { Form, FormControl, FormGroup } from '@angular/forms';
import { delay } from 'rxjs';
import { ApiError } from 'src/app/Interfaces/api-error';
import { Article } from 'src/app/Interfaces/article';
import { Blog } from 'src/app/Interfaces/blog';
import { comment } from 'src/app/Interfaces/comment';
import { commentAdd } from 'src/app/Interfaces/commentAdd';
import { Tag } from 'src/app/Interfaces/tag';
import { ArticleService } from 'src/app/services/article-service/article.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { BlogService } from 'src/app/services/blog-service/blog.service';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
    selector: 'app-article-item',
    templateUrl: './article-item.component.html',
    styleUrls: ['./article-item.component.css']
  })
export class ArticleItemComponent implements OnInit {
    
    constructor(public articleService:ArticleService, private authService: AuthService,private userService:UserService,private commentService:CommentService,private blogService:BlogService) {}

    ngOnInit(): void {
      this.commentForm = new FormControl();
      this.blogService.getBlogById(this.item.blogId).subscribe(data=>this.articleBlog=data)
      if (this.authService.isAuthenticated()) {
        this.userService.getRoles().subscribe(data=> this.isAdmin = data.includes("Admin"))
      }
      this.articleService.getArticleById(this.item.id).subscribe(data=>this.item.tags = data.tags);
    }

    @Input() item!: Article; 

    id!: number;
    blogId!: number;
    publishDate!: Date;
    body!: string;
    title!: string;
    comments!: comment[];
    tags:Tag[] = [];

    commentShow : boolean = false;
    

    commentForm! : FormControl;

    public get isLoggedIn() : boolean {
      return this.authService.isAuthenticated();
    }

    public get showComments() : boolean {
      this.commentShow = !this.commentShow;
      if (this.commentShow) {
        this.commentService.getCommentsByArticleId(this.item.id).subscribe(data => this.item.comments = data);
      }
      return this.commentShow;
    }

    updateComments() : void {
      console.log(this.item.id.toString());
      this.item.comments = [];
      this.commentService.getCommentsByArticleId(this.item.id).subscribe(data => data.forEach(val => this.item.comments.push(Object.assign({}, val))));
      console.log(this.item.comments);
    }

    sendComment(){
      let commentTextAdd = this.commentForm.value;
      let commentToAdd : commentAdd = {
        articleId : this.item.id,
        commentText : commentTextAdd
      };
      this.commentService.addComment(commentToAdd).subscribe(data=>data,(exc) => {
        alert(ApiError.StringBuilder(exc));
      });

      setTimeout(()=>{this.updateComments()}, 200);
      this.commentForm.reset();
    }

    isAdmin! : Boolean;
    articleBlog:Blog = {}
    articleUserName!:string

    get canDelete() : boolean{

      if (this.authService.isAuthenticated() && (this.isAdmin || (this.articleBlog.userId?.toString() == this.authService.getUserId()))) {
        return true;
      }
      return false;
    }

    get canEdit() : boolean{

      if (this.authService.isAuthenticated() && (this.articleBlog.userId?.toString() == this.authService.getUserId())) {
        return true;
      }
      return false;
    }

    deleteArticle()
    {
      this.articleService.deleteArticle(this.item.id).subscribe();
      window.location.reload();
    };
}
