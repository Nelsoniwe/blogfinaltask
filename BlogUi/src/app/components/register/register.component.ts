import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiError } from 'src/app/Interfaces/api-error';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  registerForm!: FormGroup;

  registerError!: string;

  ngOnInit() {
    this.registerError = '';
    this.registerForm = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      userName: new FormControl(),
      email: new FormControl(),
      password: new FormControl()
    });
  }

  register(){
    this.authService.register(this.registerForm.value).subscribe(
      () => {
        alert("Registration successful!")
        this.router.navigate(['/login']);
      },
      (exc) => {
        this.registerError = ApiError.StringBuilder(exc);
      }
    );
  }
}
