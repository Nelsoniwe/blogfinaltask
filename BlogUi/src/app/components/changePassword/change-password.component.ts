import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiError } from 'src/app/Interfaces/api-error';
import { ChangePassword } from 'src/app/Interfaces/change-password';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private authService:AuthService,private userService: UserService, private router: Router) {}
  
  ChangePasswordForm!: FormGroup;
  ChangePasswordError!:string;

  oldPassword!:string;
  newPassword!:string;
  newPasswordRepeat!:string;

  ngOnInit() {
    this.ChangePasswordError = '';
    this.ChangePasswordForm = new FormGroup({
      oldPassword: new FormControl(),
      newPassword: new FormControl(),
      newPasswordRepeat:new FormControl(),
    });
  }

  ChangePassword(){
    let changePassword:ChangePassword={
      currentPassword: this.ChangePasswordForm.value.oldPassword,
      newPassword: this.ChangePasswordForm.value.newPassword
    }
    console.log(123);
    this.userService.changePassword(changePassword).subscribe(() => {
      this.router.navigate([`/profile/${this.authService.getUserId()}`]).then(() => {
        window.location.reload();
      });
    }, (exc) => {
      this.ChangePasswordError = ApiError.StringBuilder(exc);
    });
  }
}
